# Brouillon de proposition d'atelier simple compteur connecté

## matériel nécéssaire

* un ordinateur avec une connexion wifi
* un point d'accès wifi avec internet
* une carte [wemos d1 mini](https://www.wemos.cc/en/latest/d1/d1_mini.html)
* un câble micro-usb
* un [shield bouton](https://www.wemos.cc/en/latest/d1_mini_shield/1_button.html)

## Prérequis

Aucun, mais la capacité à effectuer des tâches courantes (utiliser son système d'exploitation, un navigateur, installer et manipuler un logiciel) vous faciliteront la tâche.

Des notions d'Arduino, de programmation C et JavaScript seront nécéssaires pour adapter les programmes fournis (si vous le souhaitez).

## logiciels utilisés

### programmation de la carte

* [IDE arduino](https://www.arduino.cc/)

### collecte des données

*La collecte des données se fait dans une feuille de calcul, il existe bien sûr d'autres solutions.*

* [Google Sheets](https://www.google.com/sheets)
* [Google App Script](https://script.google.com/home)

## Objectifs

*Selon votre profil, l'objectif peut être aussi simple que **découvrir de manière totalement guidée les étapes de programmation d'une carte électronique** ou bien de plonger un peu plus loin et d'**adapter le dispositif à un projet en modifiant le code fourni**.*
